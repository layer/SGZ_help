import time
import os
import random
import pyautogui
from PIL import Image

# ==================== 基础配置 ====================
SCREEN_WIDTH, SCREEN_HEIGHT = pyautogui.size()  # 请确保模拟器分辨率为1280*720
CLICK_DELAY = (0.5, 1.5)  # 随机点击延迟范围


# ==================== 核心函数 ====================
def random_delay():
    time.sleep(random.uniform(*CLICK_DELAY))


def smart_click(x, y):
    """模拟人类点击（带随机偏移）"""
    offset_x = random.randint(-5, 5)
    offset_y = random.randint(-5, 5)
    pyautogui.moveTo(x + offset_x, y + offset_y,
                     duration=random.uniform(0.1, 0.3))
    pyautogui.click()
    random_delay()


def find_image(template_path, confidence=0.8):
    """图像识别（需要提前截取目标图片）"""
    screen = pyautogui.screenshot()
    template = Image.open(template_path)

    location = pyautogui.locate(template, screen, confidence=confidence)
    if location:
        return pyautogui.center(location)
    return None


# ==================== 自定义操作 ====================
def attack_level1_land():
    """自动攻击一级地流程"""
    pos = find_image(r'images/lv1_land.png',confidence=0.6)  # 改为相对路径
    if not pos:
        print("[警告] 未检测到一级地，请检查images目录是否包含lv1_land.png")
        return False

    smart_click(pos.x, pos.y)  # 点击土地
    random_delay()  #随机延迟

    # 识别攻击按钮
    attack_btn = find_image(r'images/attack_button.png',confidence=0.6)
    if attack_btn:
        smart_click(attack_btn.x, attack_btn.y)

        # 识别部队选择（示例逻辑需扩展）
        troops_btn = find_image(r'images/confirm_troops.png',confidence=0.6)
        if troops_btn:
            smart_click(troops_btn.x, troops_btn.y)

            #识别确认按钮
            enter_btn = find_image(r'images/confirm_button.png',confidence=0.6)
            if enter_btn:
                smart_click(enter_btn.x, enter_btn.y)
            return True
        return False

    return False


def main_loop():
    """主循环"""
    while True:
    #for i in range(3):
        # 阶段1：扫荡一级地
        # 移动鼠标寻找地块，未找到目标时继续滑动地图
        pyautogui.dragRel(20, 100, duration=1)  # 上滑地图
        time.sleep(5)  # 每5秒循环一次
        if attack_level1_land():
            time.sleep(5)  # 每5秒循环一次
            continue
        # 阶段2：收取资源（需扩展）
        # 阶段3：建筑升级（需扩展）
        #break


def check_resources():
    """检查资源完整性"""
    required_files = [
        r'images/lv1_land.png',
        r'images/attack_button.png',
        r'images/confirm_troops.png'
    ]

    missing_files = []
    for file in required_files:
        if not os.path.exists(file):
            missing_files.append(file)

    if missing_files:
        print(f"错误：缺少必要文件 ↓\n{chr(10).join(missing_files)}")
        print("请按以下要求截取游戏界面元素：")
        print("1. 一级地标识：在地图界面截取1级土地图标")
        print("2. 攻击按钮：选择土地后出现的攻击按钮（红色刀剑图标）")
        print("3. 确认部队：部队派遣界面右下角的确认按钮")
        exit(1)


def debug_image_search():
    """手动调试图像识别"""
    test_image = 'images/lv1_land.png'
    print(f"正在定位 {test_image}...")
    pos = find_image(test_image)
    if pos:
        print(f"成功定位到坐标：({pos.x}, {pos.y})")
        # 可视化验证：画红框标记位置
        screen = pyautogui.screenshot()
        screen.save('debug_screen.png')
        print("已保存带标记的截图：debug_screen.png")
    else:
        print("定位失败，请检查：")
        print("- 游戏界面是否处于主地图")
        print("- 截图是否准确显示目标元素")

# 临时启用调试
# debug_image_search()


# ==================== 使用流程 ====================
if __name__ == "__main__":
    check_resources()  # 新增验证
    print("资源验证通过，开始执行...")
    print("即将启动自动化脚本，请：")
    print("1. 启动安卓模拟器并进入游戏")
    print("2. 将游戏画面停留在主城界面")
    print("3. 5秒后开始执行...")
    time.sleep(5)

    x, y = 200, 200
    pyautogui.click(x, y)
    time.sleep(5)


    #try:
    #    pyautogui.dragRel(20, 50, duration=1)
    #except Exception as e:
    #    print(f"An error occurred: {e}")



    main_loop()
